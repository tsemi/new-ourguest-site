<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/tours", name="tours_main")
     */
    public function tourMainAction(Request $request)
    {
        return $this->render('tours/tours-main.html.twig');
    }

    /**
     * @Route("/experiences", name="experiences_main")
     */
    public function customiseExperienceAction(Request $request)
    {
        return $this->render('experiences/experiences-main.html.twig');
    }

    /**
     * @Route("/homestays", name="homestays_main")
     */
    public function homestaysMainAction(Request $request)
    {
        return $this->render('homestays/homestays-main.html.twig');
    }

    /**
     * @Route("/location", name="location_main")
     */
    public function locationMainAction(Request $request)
    {
        return $this->render('location/location.html.twig');
    }

}
